const productWrapper = document.querySelector(".product-videos");
const searchParams = new URLSearchParams(window.location.search);
const productId = searchParams.get("productId");
let firstProductContent = ` <li class="product-video">
<a href="../videos/1.mp4">النشيد الوطني</a>
</li>
<li class="product-video">
<a href="../videos/1.mp4">يلا بينا نتعلم أركان الإسلام</a>
</li>
<li class="product-video">
<a href="../videos/1.mp4">تعليم الأطفال أركان الإسلام</a>
</li>
<li class="product-video">
<a href="../videos/1.mp4">
  <span>تعليم أطفال + أناشيد إسلامية</span>
  <p>طلع البدر</p>
  <p>يا طيبة</p>
  <p>بسم الله أحلى كلام تعلمناه</p>
</a>
</li>`;
let secondProductContent = `<li class="product-video"><a href="../videos/1.mp4">الأرقام</a></li>
<li class="product-video">
  <a href="../videos/1.mp4">أيام الأسبوع 7</a>
</li>
<li class="product-video"><a href="../videos/1.mp4">الألوان</a></li>`;
let thirdProductContent = `<li class="product-video">
<a href="../videos/1.mp4">تعليم قصار الصور للأطفال</a>
</li>
<li class="product-video">
<a href="../videos/1.mp4">
  تعليم الأطفال أحاديث الرسول صلى الله عليه و سلم
</a>
</li>
<li class="product-video">
<a href="../videos/1.mp4">حديث صلاة الجماعة</a>
</li>
<li class="product-video">
<a href="../videos/1.mp4">حديث لا يؤمن أحدكم</a>
</li>`;

let fourthProductContent = `<li class="product-video"><a href="../videos/1.mp4">ريمي</a></li>
<li class="product-video"><a href="../videos/1.mp4">سنبا</a></li>`;

let fifthProductContent = ``;

switch (productId) {
  case "1":
    productWrapper.innerHTML = firstProductContent;

    break;
  case "2":
    productWrapper.innerHTML = secondProductContent;

    break;
  case "3":
    productWrapper.innerHTML = thirdProductContent;

    break;
  case "4":
    productWrapper.innerHTML = fourthProductContent;

    break;
  case "5":
    productWrapper.innerHTML = fifthProductContent;

    break;

  default:
    break;
}
